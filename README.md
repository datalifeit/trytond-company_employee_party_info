datalife_company_employee_party_info
====================================

The company_employee_party_info module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-company_employee_party_info/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-company_employee_party_info)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
