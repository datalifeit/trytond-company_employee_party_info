# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .employee import Employee


def register():
    Pool.register(
        Employee,
        module='company_employee_party_info', type_='model')
