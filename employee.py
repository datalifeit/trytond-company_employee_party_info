# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    vat_code = fields.Function(fields.Char('VAT Code'),
        'get_vat_code', searcher='search_vat_code')

    @classmethod
    def get_vat_code(cls, records, name):
        res = {}
        for record in records:
            res[record.id] = (record.party.tax_identifier.code
                if record.party.tax_identifier else None)
        return res

    @classmethod
    def search_vat_code(cls, name, clause):
        return [('party.tax_identifier', ) + tuple(clause[1:])]
